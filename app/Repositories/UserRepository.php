<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @class UserRepository
 */
class UserRepository
{
    /**
     * @var User
     */
    protected User $model;

    /**
     * Construct
     *
     * @param User $model
     */
    public function __construct( User $model )
    {
        $this->model = $model;
    }

    /**
     * @param array $filters
     * @return LengthAwarePaginator
     */
    public function getList(array $filters = []): LengthAwarePaginator
    {
        return $this->model->query()
                            ->filter($filters)
                            //->with(['coatoBranch' => function ($query) {
                            //    $query->select('id', 'name');
                            //}])
                            //->with(['coatoDepartment' => function ($query) {
                            //    $query->select('id', 'name');
                            //}])
                            ->paginate($filters['per_page'] ?? null);
    }

    /**
     * @param string $value
     * @param string $column
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getBy(string $value, string $column = 'id')
    {
        try {
            return $this->model->query()
                ->where($column, '=', $value)
                ->first();
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * @param array $values
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $values)
    {
        return $this->model->query()->create($values);
    }

    /**
     * @param $id
     * @param $values
     * @return bool|int
     */
    public function update($id, $values)
    {
        $model = $this->getBy($id);
        $model->update($values);

        return $model->refresh();
    }

    /**
     * @param string $id
     * @return bool|mixed|null
     */
    public function delete(string $id)
    {
        return $this->getBy($id)
                    ->delete();
    }
}
