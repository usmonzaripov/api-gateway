<?php

namespace App\Console\Commands;

use App\Models\CoatoBranch;
use App\Models\Fider;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class GetGeoData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billing:get-geo-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Geo Data from HET Billing';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $response = $this->request();
            $parent_code = null;
            if ($main_coato = $response['mainCoatoBranch']) {
                $parent_code = $response['mainCoatoBranch']['code'];
                $this->createCoatoBranch($main_coato);
            }
            if ($coato_branches = $response['coatoBranches']) {
                foreach ($coato_branches as $branch) {
                    $this->createCoatoBranch($branch, $parent_code);
                    $parent_code_sub = $branch['code'];
                    try {
                        $sub_items = $this->request(['regionCode' => $branch['mainCode']]);
                        foreach ($sub_items['coatoBranches'] as $item) {
                            $sub_branch = $this->createCoatoBranch($item, $parent_code_sub);
                            if ($branch['mainCode'] == '1726') {
                            $fides = $this->request(['regionCode' => $branch['mainCode'], 'districtCode' => $item['mainCode']]);
                                foreach ($fides['psFideTps'] as $fide) {
                                    Fider::query()->updateOrCreate(['code' => $fide['code']], [
                                        'coato_code' => $item['code'],
                                        'name' => $fide['name'],
                                        'capacity' => $fide['capacity'],
                                        'affiliation_id' => $fide['affiliationId'],
                                        'longitude' => $fide['lan'],
                                        'latitude' => $fide['lat'],
                                        'ps_fider_tp_type_id' => $fide['psFiderTpTypeId'],
    //                                    'upsteam_id' => $fide['upSteamFiderId'],
                                        'created_by' => 1,
                                        'upsteam_id' => $fide['upSteamFiderId'],
                                        'legal_meters' => $fide['legalMeters'],
                                        'household_meters' => $fide['householdMeters']
                                    ]);
                                }
                            }
                        }
                    }
                    catch (\Exception $exception) {
                        $this->warn($exception->getMessage());
                    }
                }
            }
        }
        catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }

        $this->info('Get data from HET Billing completed!');
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request($query = []): array
    {
        $token = env('HET_BILLING_AUTH_TOKEN');
        $endpoint = env('HET_MAP_ENDPOINT');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '.$token,
            'Accept' => 'application/json',
        ];
        $response = $client->request('GET', $endpoint, [
            'headers' => $headers,
            'query' => $query
        ]);
        $responseBody = $response->getBody()->getContents();

        return json_decode($responseBody, true);
    }

    private function createCoatoBranch($coato, $parent_code = null)
    {
        return CoatoBranch::query()->updateOrCreate(
            ['code' => $coato['code']],
            [
                'name' => $coato['name'],
                'lat' => $coato['latitude'],
                'lon' => $coato['longitude'],
                'created_by' => 1,
                'number_branches' => $coato['countCoatoBranches'],
                'number_legal_consumers' => $coato['countLegalConsumers'],
                'number_household_consumers' => $coato['countHouseholdConsumers'],
                'number_substations' => $coato['countPS'],
                'number_tp' => $coato['countTP'],
                'parent_code' => $parent_code
            ]
        );
    }

}
