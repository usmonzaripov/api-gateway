<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Utils;

/**
 * @class ServiceRoutingController
 * @description Gateway route controller
 */
class ServiceRoutingController extends Controller
{
    use ApiResponse;

    /**
     * @param string $service
     * @param string $endpoint
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(string $service, string $endpoint)
    {
        try {
            $services = config('services.internal_services');
            if (!array_key_exists($service, $services)) {
                return $this->replyRaw(['message' => 'The service not found in internal services list!'], 500);
            }
            $url = $services[$service] . '/' . $endpoint;
            $headers_raw = getallheaders();
            $headers = array_filter($headers_raw, function ($value) {
                return ($value !== NULL && $value !== FALSE && $value !== '');
            });
            $client = new Client();
            $multipart = [];
            foreach (\request()->all() as $key => $value) {
                if (\request()->hasFile($key)) {
                    if(($file=\request()->file($key)) instanceof \Illuminate\Http\UploadedFile){
                        /** @var $file \Illuminate\Http\UploadedFile*/
                        $multipart[] = [
                            'name' => $key,
                            'contents' => Utils::tryFopen($file->path(), 'r'),
                            'filename' => $file->getClientOriginalName(),
                        ];
                    }
                    foreach (\request()->file($key) as $file) {
                        $multipart[] = [
                            'name' => $key,
                            'contents' => Utils::tryFopen($file->path(), 'r'),
                            'filename' => $file->getClientOriginalName(),
                        ];
                    }
                } else {
                    $multipart[] = [
                        'name' => $key,
                        'contents' => $value
                    ];
                }
            }
            $multipart = \request()->getContentTypeFormat() == 'form' ? $multipart : [];
            if (\request()->getContentTypeFormat() == 'form') {
                $params = [
                    'multipart' => $multipart,
                ];
            } else {
                $params = [
                    'query' => \request()->query(),
                    'payload' => \request()->getPayload(),
                    'json' => \request()->all(),
                ];
            }
            $params['header']=$headers;
            $response = $client->request(\request()->method(), $url, $params);
            $contentType = $response->getHeader('Content-Type')[0];
            if ($contentType == "application/json") {
                return $this->replyRaw(json_decode($response->getBody()->getContents(), true), $response->getStatusCode());
            }
            return response($response->getBody()->getContents(), $response->getStatusCode(), $response->getHeaders());
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
            return $this->replyRaw(json_decode($response->getBody()->getContents(), true), $response->getStatusCode());
        }
    }

    /**
     * @param string $service
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function document(string $service)
    {
        try {
            $services = config('services.internal_services');
            if (!array_key_exists($service, $services)) {
                return $this->replyRaw(['message' => 'The service not found in internal services list!'], 500);
            }
            $url = $services[$service] . '/docs/api-docs.json';
            $client = new Client();
            $response = $client->request(\request()->method(), $url);

            return $this->replyRaw(json_decode(str_replace('/api', '/' . $service . '/api', $response->getBody()->getContents()), true), $response->getStatusCode());
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
            return $this->replyRaw(json_decode($response->getBody()->getContents(), true), $response->getStatusCode());
        }
    }
}
