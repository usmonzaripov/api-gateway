<?php

namespace App\Http\Controllers\API\V1;

class InfoController
{
    /**
     * @OA\Get(path="/api/v1/info",
     *     tags={"Info"},
     *     summary="The info request and response",
     *     description="Desc...",
     *     operationId="getUsers",
     *     security={ {"bearerAuth": {} }},
     * @OA\Response(
     *         response=200,
     *         description="Success response.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     * )
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response([
            'app_name' => 'CRM GATEWAY',
            'version' => 1.0
        ]);
    }
}
