<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Response;


/**
 * @class User Controller
 */
class UserController extends Controller
{
    /**
     * @property UserService $service
     */
    private UserService $service;

    /**
     * Construct
     */
    public function __construct( UserService $service )
    {
        $this->service = $service;
    }

    /**
     * @OA\Get(path="/api/v1/users",
     *     tags={"Users"},
     *     summary="Returns User list",
     *     description="Returns list. You can get items with filters",
     *     operationId="getUserList",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *          name="search",
     *          description="Filter. Key value for search",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *              example=""
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_column",
     *          description="Filter. Between date column.",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *              example=""
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_from",
     *          description="Filter. Between from date",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="datetime",
     *              example=""
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_to",
     *          description="Filter. Between to date",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="datetime",
     *              example=""
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="order_column",
     *          description="Sort. Column name.",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *              example="created_at"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="order_type",
     *          description="Sort. Type.",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *              example="desc"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="per_page",
     *          description="Pagination. Per page value.",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="integer",
     *              example="15"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     * )
     *
     * Display a listing of the resource.
     *
     * @param UserRequest $request
     * @return Response
     */
    public function index( UserRequest $request ): Response
    {
        return $this->replySuccess(
                trans('messages.success'),
                $this->service->getList($request->validated())
        );
    }

    /**
     * @OA\Post(path="/api/v1/users",
     *     tags={"Users"},
     *     summary="Store",
     *     description="Create single.",
     *     operationId="storeUser",
     *     security={ {"bearerAuth": {} }},
     *      @OA\Parameter(
     *          name="name",
     *          description="Name",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="John Doe"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="test@email.com"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="Password",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="qwerty"
     *          )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Content.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     )
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param  UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request): Response
    {

        return $this->replySuccess(
                trans('messages.created'),
                $this->service->store($request->validated())->toArray()
        );
    }

    /**
     * @OA\Get(path="/api/v1/users/{id}",
     *     tags={"Users"},
     *     summary="Get one item",
     *     description="Get single.",
     *     operationId="getUser",
     *     security={ {"bearerAuth": {} }},
     *      @OA\Parameter(
     *          name="id",
     *          description="ID",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example=""
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     )
     * )
     *
     * Display the specified resource.
     *
     * @param UserRequest $request
     * @param  string  $id
     * @return Response
     */
    public function show(UserRequest $request, string $id): Response
    {
        $user = $this->service->getOne($id);
        return $this->replySuccess(
                trans('messages.model_found'),
                $user?$user->toArray():[]
        );
    }

    /**
     * @OA\Put(path="/api/v1/users/{id}",
     *     tags={"Users"},
     *     summary="Update",
     *     description="Update single.",
     *     operationId="updateUser",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *          name="id",
     *          description="ID",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="1"
     *          )
     *      ),
     *    @OA\Parameter(
     *          name="name",
     *          description="Name",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="John Doe"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="test@email.com"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="Password",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="qwerty"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Content.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  UserRequest  $request
     * @param  string  $id
     * @return Response
     */
    public function update(UserRequest $request, string $id): Response
    {
        return $this->replySuccess(
                trans('messages.updated'),
                $this->service->update($id, $request->validated())->toArray()
        );
    }

    /**
     * @OA\Delete(path="/api/v1/users/{id}",
     *     tags={"Users"},
     *     summary="Delete item",
     *     description="Delete single item",
     *     operationId="deleteUser",
     *     security={ {"bearerAuth": {} }},
     *      @OA\Parameter(
     *          name="id",
     *          description="ID",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              example="1"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found.",
     *         @OA\Schema(
     *             additionalProperties={
     *                 "type": "integer",
     *                 "format": "int32"
     *             }
     *         )
     *     ),
     * )
     * Remove the specified resource from storage.
     *
     * @param UserRequest $request
     * @param  string  $id
     * @return Response
     */
    public function destroy(UserRequest $request, string $id): Response
    {
        $this->service->delete($id);

        return $this->replySuccess(trans('messages.deleted'));
    }
}
