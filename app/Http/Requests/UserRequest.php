<?php

namespace App\Http\Requests;

use App\Traits\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Validation\Rule;

/**
 * @class UserRequest request validator
 */
class UserRequest extends FormRequest
{
    use ApiRequest;

    /**
     * @var string $route_parameter
     */
    protected static string $route_parameter = 'user';

    /**
     * @private array $rules
     * @return array
     */
    private function rulesList(): array
    {
        return [
                'index' => [
                        'search'        => 'nullable|min:2',
                        'date_column'   => 'nullable|in:created_at,updated_at',
                        'date_from'     => 'required_if:date_column,!=,null|date',
                        'date_to'       => 'required_if:date_column,!=,null|date|after:date_from',
                        'order_column'  => 'nullable|required_if:order_type,!=,null|in:id,created_at,updated_at',
                        'order_type'    => 'nullable|required_if:order_column,!=,null|in:asc,desc',
                        'per_page'      => 'nullable|integer'
                ],
                'store' => [
                        'name' => 'required|min:3',
                        'email' => 'required|unique:users,email|email',
                        'password' => 'required|min:6|max:200',
                        'coato_branch_id'=>'nullable|exists:coato_branches,id',
                        'coato_department_id'=>'nullable|exists:coato_departments,id'
                ],
                'update' => [
                        'name' => 'required|min:3',
                        'id'   => 'required|exists:users',
                        'email' => [
                                'required',
                                Rule::unique('users', 'email')
                                        ->ignore($this->email, 'email')
                                //                        ->whereNull('deleted_at')
                        ],
                        'password' => 'required|min:6|max:200',
                        'coato_branch_id'=>'nullable|exists:coato_branches,id',
                        'coato_department_id'=>'nullable|exists:coato_departments,id'
                ],
                'show' => [
                    //@TODO After integration soft delete change the logic
                    //                'id' => 'required|exists:users,id,deleted_at,NULL',
                    'id' => 'required|exists:users,id',
                ],
                'destroy' => [
                    //@TODO After integration soft delete change the logic
                    //'id' => 'required|exists:users,id,deleted_at,NULL',
                    'id' => 'required|exists:users,id',
                ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return $this->rulesList()[$this->route()->getActionMethod()];
    }
}
