<?php

namespace App\Services;

use App\Models\Ticket;
use App\Repositories\TicketRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * @class TicketService
 */
class TicketService
{
    /**
     * @var TicketRepository
     */
    protected TicketRepository $repository;

    protected AttachmentUpload $uploadService;
    /**
     * @param TicketRepository $repository
     */
    public function __construct(TicketRepository $repository, AttachmentUpload $uploadService)
    {
        $this->repository = $repository;
        $this->uploadService = $uploadService;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getList(array $filters = [])
    {
        return $this->repository->getList($filters)->toArray();
    }

    /**
     * @param int $key
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOne(int $key)
    {
        return $this->repository->getBy($key);
    }

    /**
     * @param $values
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($values)
    {
        $values['creator_id'] = Auth::id();

        $ticket = $this->repository->create($values);

        if (isset($values['files']) && !in_array(null,$values['files'],true))
        {
            foreach($values['files'] as $file)
            {
                $this->uploadService->uploadFile($ticket->id,Ticket::class,$file);
            }
        }

        return $ticket;
    }

    /**
     * @param int $id
     * @param array $values
     * @return Model
     */
    public function update(int $id, array $values)
    {
        return $this->repository->update($id, $values);
    }

    /**
     * @param int $id
     * @return bool|mixed|null
     */
    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param $values
     * @return \Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public function assignTicketUsersStore($id, $values)
    {
        return $this->repository->assignTicketUsers($id, $values);
    }

}
