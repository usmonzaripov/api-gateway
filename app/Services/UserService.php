<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

/**
 * @class UserService
 */
class UserService
{
    /**
     * @var UserRepository
     */
    protected UserRepository $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct( UserRepository $repository )
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getList(array $filters = [])
    {
        return $this->repository->getList($filters)->toArray();
    }

    /**
     * @param string $key
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOne(string $key)
    {
        return $this->repository->getBy($key);
    }

    /**
     * @param $values
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($values)
    {
        $values['password'] = Hash::make($values['password']);
        return $this->repository->create($values);
    }

    /**
     * @param string $id
     * @param array $values
     * @return Model
     */
    public function update(string $id, array $values)
    {
        $values['password'] = Hash::make($values['password']);
        return $this->repository->update($id, $values);
    }

    /**
     * @param string $id
     * @return bool|mixed|null
     */
    public function delete(string $id)
    {
        return $this->repository->delete($id);
    }

}
