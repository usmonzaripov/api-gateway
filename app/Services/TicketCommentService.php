<?php

namespace App\Services;

use App\Repositories\TicketCommentRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * @class TicketCommentService
 */
class TicketCommentService
{
    /**
     * @var TicketCommentRepository
     */
    protected TicketCommentRepository $repository;

    /**
     * @param TicketCommentRepository $repository
     */
    public function __construct( TicketCommentRepository $repository )
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getList(array $filters = [])
    {
        return $this->repository->getList($filters)->toArray();
    }

    /**
     * @param int $key
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOne(int $key, int $ticket_id)
    {
        return $this->repository->getBy($key, $ticket_id);
    }

    /**
     * @param $values
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($values)
    {
        $values['type'] = $this->getType($values['type']);
        return $this->repository->create($values);
    }

    /**
     * @param int $id
     * @param array $values
     * @return Model
     */
    public function update(int $id, array $values)
    {
        $values['type'] = $this->getType($values['type']);
        return $this->repository->update($id, $values);
    }

    /**
     * @param int $id
     * @return bool|mixed|null
     */
    public function delete(int $id, int $ticket_id)
    {
        return $this->repository->delete($id, $ticket_id);
    }

    /**
     * Get the value of the "type" attribute.
     *
     * @param  string  $value
     * @return string|int
     */
    public function getType($value)
    {
        return $this->repository->getTypeAttribute($value);
    }

}
