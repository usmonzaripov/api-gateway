<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * @class AttachmentUpload
 */
class AttachmentUpload
{
    /**
     * @var AttachmentService
     */
    protected AttachmentService $service;

    /**
     * @param AttachmentService $service
     */
    public function __construct( AttachmentService $service )
    {
        $this->service = $service;
    }

    /**
     * @param int $modelId
     * @param string $modelName
     * @param $file
     * @return Builder|Model
     */
    public function uploadFile(int $modelId, string $modelName,UploadedFile $file): Model|Builder
    {
        $values = ['model_id' => $modelId, 'model_type' => $modelName,'file' => $file];

        return $this->service->store($values);
    }

}
