<?php

namespace App\Services;

use App\Http\Requests\FiderRequest;
use App\Models\Affiliation;
use App\Models\FiderType;
use App\Repositories\FiderRepositoy;

/**
 * @class FiderService
 */
class FiderService
{
    /**
     * @var FiderRepositoy
     */
    protected FiderRepositoy $repository;

    public function __construct(FiderRepositoy $fiderRepositoy)
    {
        $this->repository = $fiderRepositoy;
    }

    /**
     * @return []
     */
    public function getItems(FiderRequest $request)
    {
        return $this->repository->getList($request->get('coato_code'));
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return FiderType::query()->get()->all();
    }

    /**
     * @return array
     */
    public function getAffiliations()
    {
        return Affiliation::query()->get()->all();
    }

}
