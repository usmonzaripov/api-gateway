<?php

namespace App\Services;

use App\Repositories\CoatoDepartmentRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * @class CoatoDepartmentService
 */
class CoatoDepartmentService
{
    /**
     * @var CoatoDepartmentRepository
     */
    protected CoatoDepartmentRepository $repository;

    /**
     * @param CoatoDepartmentRepository $repository
     */
    public function __construct( CoatoDepartmentRepository $repository )
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getList(array $filters = [])
    {
        return $this->repository->getList($filters)->toArray();
    }

    /**
     * @param int $key
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOne(int $key)
    {
        return $this->repository->getBy($key);
    }

    /**
     * @param $values
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($values)
    {
        $values['created_by'] = Auth::id();
        return $this->repository->create($values);
    }

    /**
     * @param int $id
     * @param array $values
     * @return Model
     */
    public function update(int $id, array $values)
    {
        return $this->repository->update($id, $values);
    }

    /**
     * @param int $id
     * @return bool|mixed|null
     */
    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

}
