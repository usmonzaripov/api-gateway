<?php

namespace App\Services;

use App\Repositories\AttachmentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * @class AttachmentService
 */
class AttachmentService
{
    /**
     * @var AttachmentRepository
     */
    protected AttachmentRepository $repository;

    /**
     * @param AttachmentRepository $repository
     */
    public function __construct( AttachmentRepository $repository )
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getList(array $filters = [])
    {
        return $this->repository->getList($filters)->toArray();
    }

    /**
     * @param int $key
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOne(int $key)
    {
        return $this->repository->getBy($key);
    }


    /**
     * @param $values
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store($values)
    {
        $values = $this->generateFileName($values);
        $values['created_by'] = Auth::id();

        return $this->repository->create($values);
    }

    /**
     * @param int $id
     * @param array $values
     * @return bool|int
     */
    public function update(int $id, array $values)
    {
        if (isset($values['file']) and file_exists($values['file']))
        {
          $values = $this->generateFileName($values);
        }

        return $this->repository->update($id, $values);
    }

    /**
     * @param int $id
     * @return bool|mixed|null
     */
    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param int $modelId
     * @param string $modelName
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByModel(int $modelId,string $modelName)
    {
        return $this->repository->getFileByModel($modelId,$modelName);
    }

    /**
     * @param array $values
     * @return array
     */
    private function generateFileName(array $values): array
    {
        $file = $values['file'];

        $fileExtension = $file->getClientOriginalExtension();
        $baseName = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
        $fileName = "{$baseName}-" . uniqid() . ".{$fileExtension}";
        $filePath = $file->storeAs('attachments/'.class_basename($values['model_type']), $fileName);

        $newValues = [
            'file' => $filePath,
            'file_name' => $fileName,
            'file_ext' => $fileExtension,
        ];

        $newValues = array_merge($values,$newValues);

        return $newValues;
    }

}
