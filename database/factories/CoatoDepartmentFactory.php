<?php

namespace Database\Factories;

use App\Models\CoatoDepartment;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CoatoDepartment>
 */
class CoatoDepartmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CoatoDepartment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'coato_branch_id' => function () {
                return \App\Models\CoatoBranch::factory()->create()->id;
            },
            'code' => $this->faker->unique()->numerify('DEP###'),
            'name' => $this->faker->word,
            'created_by' => function () {
                return \App\Models\User::factory()->create()->id;
            },
            'status' => $this->faker->boolean(90), // 90% chance of being true
        ];
    }
}
