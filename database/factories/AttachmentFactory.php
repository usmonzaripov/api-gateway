<?php

namespace Database\Factories;

use App\Models\Attachment;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attachment>
 */
class AttachmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attachment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_by' => \App\Models\User::factory(),
            'model_id' => $this->faker->numberBetween(1, 10),
            'model_type' => $this->faker->randomElement(['Ticket', 'Task', 'Other']),
            'file' => $this->faker->word . '.' . $this->faker->fileExtension,
            'file_name' => $this->faker->word,
            'file_ext' => $this->faker->fileExtension,
        ];
    }
}
