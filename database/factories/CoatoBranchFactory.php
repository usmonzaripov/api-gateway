<?php

namespace Database\Factories;

use App\Models\CoatoBranch;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CoatoBranch>
 */
class CoatoBranchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CoatoBranch::class;


    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code' => $this->faker->unique()->numerify('BR###'),
            'name' => $this->faker->company,
            'created_by' => function () {
                return \App\Models\User::factory()->create()->id;
            },
            'updated_by' => $this->faker->date,
            'status' => $this->faker->boolean(90), // 90% chance of being true
        ];
    }
}
