<?php

namespace Database\Factories;

use App\Models\Affiliation;
use App\Models\Fider;
use App\Models\FiderType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Fider>
 */
class FiderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ps_fider_tp_type_id' => (FiderType::query()->select(['id'])->inRandomOrder()->first()->getAttribute('id')) ?? null,
            'upsteam_id' => (Fider::query()->select(['id'])->inRandomOrder()->first()?->getAttribute('id')) ?? null,
            'code' => $this->faker->countryCode(),
            'name' => $this->faker->streetName(),
            'affiliation_id' => (Affiliation::query()->select(['id'])->inRandomOrder()->first()?->getAttribute('id')) ?? null,
            'capacity' => $this->faker->randomDigitNotZero(),
            'coato_code' => $this->faker->countryCode,
            'plans_index' => $this->faker->randomDigit(),
            'load_index' => $this->faker->randomDigit(),
            'mba' => $this->faker->randomDigit(),
            'number_application' => $this->faker->randomDigit(),
            'year_commissioning' => $this->faker->dateTime(),
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude,
            'status' => 1,
            'created_by' => 1,
            'updated_by' => 1
        ];
    }
}
