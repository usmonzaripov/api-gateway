## About project
What is Gateway? An API gateway is set up in front of the microservices and becomes the entry point for every
new request being executed by the app.

### Stacks
1. <a href="https://www.php.net/releases/8.2/en.php" target="_blank">PHP-8.2<a/>. Framework <a href="https://laravel.com/docs/10.x/releases" target="_blank">Laravel 10</a>.
2. <a href="https://www.postgresql.org/about/news/postgresql-14-released-2318/" taget="_blank">PostgreSQL-16.1v</a>.
3. <a href="https://nginx.org/en/download.html">Nginx-latest</a>.
### Installation guide
Before installation set up your <a href="https://docs.gitlab.com/ee/user/ssh.html" target="_blank">SSH keys on GitLab</a> and <a href="https://docs.docker.com/engine/install/ubuntu/" target="_blank">Docker</a>.
<br />
### Steps (with Docker):
``$ git clone https://u-gitlab.uzinfocom.uz/uzinfocom-erp/crm-backend/crm-gateway.git``
<br />
``$ cd crm-gateway``
<br />
``$ cp .env.example .env``
<br />
``$ docker-compose up --build -d``
<br />
``$ open http://localhost:7780``
<br />

### Docker network settings
``$ docker network create crm-gateway`` <br />
``$ docker network connect crm-gateway gateway-web`` <br />
``$ docker network connect crm-gateway gateway-php`` <br />
``$ docker network connect crm-gateway crm-web`` <br />
``$ docker network connect crm-gateway call-center-web`` <br />
``$ docker network connect crm-gateway help-desk-web`` <br />

### Docker container's shell
PHP:
<br />
``$ docker exec -it crm-php /bin/sh``
<br />
Database:
<br />
``$ docker exec -it crm-database /bin/sh``

### Swagger documentation for API
<a href="https://zircote.github.io/swagger-php/guide/" target="_blank">PHP Swagger guide.</a>
<br />
Local URL:
<br />
http://localhost:7777/api/documentation
<br />
Aritsan command for regenerate:
<br />
1. ``$ docker exec -it crm-php /bin/sh``
2. ``$ php artisan l5-swagger:generate``
3. ``$ docker exec -it crm-php php artisan l5-swagger:generate``

