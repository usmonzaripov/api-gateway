<?php
return [
        'created' => 'Successfully created',
        'updated' => 'Successfully updated',
        'deleted' => 'Successfully deleted',
        'model_found' => 'Model found',
        'success'=> "Operation successful"
];
