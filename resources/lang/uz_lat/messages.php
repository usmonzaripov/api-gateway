<?php
return [
        'created' => 'Muvaffaqiyatli yaratildi',
        'updated' => 'Muvaffaqiyatli yangilandi',
        'deleted' => 'Muvaffaqiyatli o\'chirildi',
        'model_found' => 'Model topildi',
        'success' => 'Amal muvaffaqiyatli bajarildi'
];
