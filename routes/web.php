<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//About
Route::get('/', function () {
    return response([
        'app_name' => 'CRM GATEWAY SERVER',
        'version' => 1.0
    ]);
});
//Internal services docs control
Route::get('/document/{service}', [\App\Http\Controllers\ServiceRoutingController::class, 'document']);
//Internal services route control
Route::middleware(['auth:sanctum'])->group(function () {
    Route::any('/{service}/{endpoint?}', [\App\Http\Controllers\ServiceRoutingController::class, 'handle'])
        ->where('endpoint', '(.*)');;
});
