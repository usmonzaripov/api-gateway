<?php

use App\Http\Controllers\API\V1\AttachmentController;
use App\Http\Controllers\API\V1\BoardColumnController;
use App\Http\Controllers\API\V1\BoardController;
use App\Http\Controllers\API\V1\CoatoBranchController;
use App\Http\Controllers\API\V1\LoginController;
use App\Http\Controllers\API\V1\ProjectController;
use App\Http\Controllers\API\V1\ProjectStatusController;
use App\Http\Controllers\API\V1\TicketController;
use App\Http\Controllers\API\V1\TicketStatusController;
use App\Http\Controllers\API\V1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('v1')->group(function () {
    Route::get('info', [\App\Http\Controllers\API\V1\InfoController::class, 'index']);

    Route::apiResource('users', \App\Http\Controllers\API\V1\UserController::class);
    Route::post('/user/login', [LoginController::class, 'login']);
    Route::middleware(['auth:sanctum'])->get('/user/profile', function (Request $request) {
        return $request->user();
    });
});



